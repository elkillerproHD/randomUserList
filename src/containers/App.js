import React from 'react';
import styled from "styled-components";
import {Header} from "../components/header";
import {Grid} from "../components/grid";
import json from '../api.json'

const Container = styled.div `
  width: 100vw;
  height: 100vh;
  display: flex;
  justify-content: center;
  align-items: center;
`
const SubContainer = styled.div `
  width: 45vw;
  height: 80vh;
  display: flex;
  flex-direction: column;
  @media (max-width: 1000px) {
      width: 55vw;
  }
  @media (max-width: 860px) {
      width: 65vw;
  }
  @media (max-width: 680px) {
      width: 75vw;
  }
  @media (max-width: 550px) {
      width: 85vw;
  }
`

export default class App extends React.Component {
  state = {
    rows: [],
    allRows: [],
    male: false,
    female: false,
    email: '',
    firstLoad: true
  }
  Reload = async () => {
    const { state } = this;
    return new Promise(async (resolve) => {
      fetch('https://randomuser.me/api/?results=50')
        .then((response) => {
          return response.json();
        })
        .then((myJson) => {
          console.log(myJson.results)
          if(state.firstLoad){
            this.setState({
              rows: myJson.results,
              allRows: myJson.results,
              firstLoad: false
            })
          } else {
            this.setState({
              rows: myJson.results,
              allRows: myJson.results,
              male: false,
              female: false,
              email: ''
            })
          }
          resolve()
        }).catch(() => {
          alert('Hubo un error en la query de RandomUser')
          resolve()
      })
    })

  }
  SetMale = () => {
    const { allRows, male } = this.state;
    if(male){
      this.setState({
        male: false,
        rows: allRows,
      })
    } else {
      let rows = []
      allRows.map((r) => {
        if(r.gender === 'male') {
          rows.push(r)
        }
      })
      console.log(rows)
      this.setState({
        rows,
        female: false,
        male: true,
        email: ''
      })
    }
  }
  SetFemale = () => {
    const { allRows, female } = this.state;
    if(female){
      this.setState({
        female: false,
        rows: allRows,
      })
    } else {
      let rows = []
      allRows.map((r) => {
        if(r.gender === 'female') {
          rows.push(r)
        }
      })
      console.log(rows)
      this.setState({
        rows,
        male: false,
        female: true,
        email: ''
      })
    }
  }
  SearchEmail = (email) => {
    const { allRows } = this.state
    const rows = []
    allRows.map(query => {if(query.email.startsWith(email)){ rows.push(query) }});
    this.setState({
      rows
    })
    // startsWith()
  }
  SetEmailValue = (email) => {
    this.setState({
      email,
      male: false,
      female: false,
    })
    this.SearchEmail(email)
  }
  componentDidMount() {
    // console.log(json)
    const queryString = window.location.search;
    console.log(window.location.pathname.split('/'))
    const urlParams = new URLSearchParams(queryString);
    const gender = urlParams.get('gender')
    const path = window.location.pathname.split('/')[1]
    console.log(path.length)
    if(path.length > 0){
      this.Reload().then(() => {
        this.SetEmailValue(path)
      })
    } else {
      if(gender === 'M'){
        this.Reload().then(() => {
          this.SetMale()
        })
        return
      } else if(gender === 'F'){
        this.Reload().then(() => {
          this.SetFemale()
        })
        return
      }
      this.Reload();
    }
  }


  render() {
    const { state } = this;
    return (
      <Container>
        <SubContainer>
          <Header
            SetMale={this.SetMale}
            male={state.male}
            SetFemale={this.SetFemale}
            female={state.female}
            SetEmailValue={this.SetEmailValue}
            email={state.email}
            Reload={this.Reload}
          />
          <Grid rows={state.rows} />
        </SubContainer>
      </Container>
    )
  }
}
