import * as React from "react"

function SvgTriangle(props) {
  return (
    <svg viewBox="0 0 512 512" {...props}>
      <path
        d="M507.521 84.606L282.655 459.383c-12.074 20.122-41.237 20.122-53.311 0L4.479 84.606c-12.433-20.72 2.493-47.08 26.655-47.08h449.732c24.163 0 39.089 26.36 26.655 47.08z"
        data-original="#000000"
        className="prefix__active-path"
      />
    </svg>
  )
}

export default SvgTriangle
