import React, { useState } from 'react'
import styled from "styled-components";
import Ripples from 'react-ripples'
import SvgTriangle from "../../../svg/Triangle";
import SvgUser from "../../../svg/User";
import SvgEmail from "../../../svg/Mail";
import SvgBirthday from "../../../svg/Bithday";
import SvgLocation from "../../../svg/Location";
import SvgPhone from "../../../svg/Phone";

const Container = styled.div `
  width: 45vw;
  height: 10vh;
  border: 1px solid #d2d2d2;
  cursor: pointer;
  background-color: white;
  position: relative;
  display: flex;
  justify-content: space-between;
  align-items: center;
  @media (max-width: 1000px) {
      width: 55vw;
  }
  @media (max-width: 860px) {
      width: 65vw;
  }
  @media (max-width: 680px) {
      width: 75vw;
  }
  @media (max-width: 550px) {
      width: 85vw;
  }
`
const Avatar = styled.img `
  width: 7vh;
  height: 7vh;
  border-radius: 50px;
  border: 1px solid #d2d2d2;
  margin-left: 3%;
`
const BigAvatar = styled.img `
  width: 20vh;
  height: 20vh;
  border-radius: 20vh;
  border: 1px solid #d2d2d2;
  margin-left: 3%;
  margin-top: 4vh;
`
const TextEmail = styled.p `
  font-size: 12px;
  color: #d2d2d2;
`
const TextName = styled.p `
  color: #000;
  font-size: 15px;
`
const ContainerInfo = styled.div `
  width: 45vw;
  border: 1px solid #d2d2d2;
  cursor: pointer;
  background-color: white;
  position: relative;
  display: flex;
  flex-direction: column;
  justify-content: flex-start;
  align-items: center;
  @media (max-width: 1000px) {
      width: 55vw;
  }
  @media (max-width: 860px) {
      width: 65vw;
  }
  @media (max-width: 680px) {
      width: 75vw;
  }
  @media (max-width: 550px) {
      width: 85vw;
  }
`
const BigText1 = styled.p `
  color: #d2d2d2;
  font-size: 16px;
  margin: 14px 0 0;
`
const BigText2 = styled.p `
  color: #000;
  font-size: 26px;
  margin: 10px 0 0;
`
const IconRowContainer = styled.div `
  width: 75%;
  display: flex;
  justify-content: space-between;
  align-items: center;
  margin-top: 7%;
`

const strings = {
  name: 'Hi, My name is',
  email: 'My email address is',
  birth: 'My birthday is',
  location: 'My address is',
  phone: 'My phone number is'
}

export const Row = (props) => {
  const [open, setOpen] = useState(true)
  const [firstOpen, setFirstOpen] = useState(false)
  const [text1, setText1] = useState('Hi, My name is')
  const [text2, setText2] = useState(`${props.name.first} ${props.name.last}`)
  const [iconHovered, setIconHovered] = useState(0)
  const RowPress = () => {
    console.log("Row Press")
    if (!firstOpen) {
      setFirstOpen(true)
    } else {
      setOpen(!open)
    }
  }
  const HoverUser = (index) => {
    setText1(strings.name)
    setText2(`${props.name.first} ${props.name.last}`)
    setIconHovered(index)
  }
  const EmailUser = (index) => {
    setText1(strings.email)
    setText2(props.email)
    setIconHovered(index)
  }
  const BirthDayUser = (index) => {
    setText1(strings.birth)
    setText2(props.dob.date.substring(0, (props.dob.date.indexOf("T"))))
    setIconHovered(index)
  }
  const LocationUser = (index) => {
    setText1(strings.location)
    setText2(`${props.location.street.number} ${props.location.street.name}`)
    setIconHovered(index)
  }
  const PhoneUser = (index) => {
    setText1(strings.phone)
    setText2(props.phone)
    setIconHovered(index)
  }
  const Icons = [
    {
      icon: SvgUser,
      hover: HoverUser
    },
    {
      icon: SvgEmail,
      hover: EmailUser
    },
    {
      icon: SvgBirthday,
      hover: BirthDayUser
    },
    {
      icon: SvgLocation,
      hover: LocationUser
    },
    {
      icon: SvgPhone,
      hover: PhoneUser
    },
  ]
    return [
      <Container>
        <Ripples>
          <Container onClick={RowPress} >
            <Avatar src={props.picture.thumbnail} />
            <TextName >{`${props.name.first} ${props.name.last}`}</TextName>
            <TextEmail >{`@${props.email}`}</TextEmail>
            <SvgTriangle style={{ marginRight: '5%' }} width={20} height={20} fill={'#000'} />
          </Container>
        </Ripples>
      </Container>,
      firstOpen && ( <ContainerInfo className={open ? 'ContainerInfoOpen' : 'ContainerInfoClose'} >
        <BigAvatar src={props.picture.large} />
        <BigText1>{text1}</BigText1>
        <BigText2>{text2}</BigText2>
        <IconRowContainer>
          {
            Icons.map((Icn, index) => {
              if(index === iconHovered){
                return (
                  <Icn.icon
                    width={25}
                    height={25}
                    className={'scale-up-ver-bottom'}
                    fill={'#000'}
                  />
                )
              } else {
                return (
                  <Icn.icon
                    onMouseEnter={() => Icn.hover(index)}
                    width={25}
                    height={25}
                    fill={'#d2d2d2'}
                  />
                )
              }
            } )
          }
        </IconRowContainer>
      </ContainerInfo>)
    ]



}
