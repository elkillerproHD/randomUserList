import React from 'react'
import styled from "styled-components";
import {Row} from "./row/index";

const Container = styled.div `
  width: 102%;
  height: 80vh;
  overflow: auto;
  overflow-x: hidden;
  padding-right:  4%;
  padding-bottom:  4%;
  margin-top: 8%;
`

export const Grid = ( props ) => {
  // const json = JSON.parse(Json);
  const Rows = props.rows.map(r => <Row {...r} />)
  return (
    <Container>
      { Rows }
    </Container>
  )
}
