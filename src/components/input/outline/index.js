import React from "react";
import styled from "styled-components";
import TextField, { Input } from '@material/react-text-field';

const Container = styled.div `
  width: ${({width}) => width};
  position: relative;
`
export const OutLineTextField = (props) => {
  return(
    <Container width={props.width}>
      <TextField
        label={props.label}
        outlined
      ><Input
        value={props.value}
        style={{
          width: props.width,
          color: '#d2d2d2',
        }}
        onChange={props.onChangeText} />
      </TextField>
    </Container>
  )
}
OutLineTextField.defaultProps = {
  width: "100%",
  onChangeText: () => {},
  value: '',
  label: 'CHANGE LABEL'
}
