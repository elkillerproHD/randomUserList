import React from "react";
import styled from "styled-components";
import Ripples from 'react-ripples'
import SvgFemale from "../../svg/Female";
import SvgMale from "../../svg/Male";
import {OutLineTextField} from "../input/outline";
import { SvgRefresh } from "../../svg/Refresh";

const Container = styled.div `
  width: 100%;
  display: flex;
  justify-content: space-between;
  align-items: center;
  @media (max-width: 400px) {
    flex-direction: column;
  }
`
const LeftCol = styled.div `
  display: flex;
  justify-content: flex-start;
  align-items: center;
  width: 45%;
  @media (max-width: 400px) {
    width: 100%;
    justify-content: center;
  }
`
const EmptySpace = styled.div`
  width: 8%;
`
const RightCol = styled.div `
  display: flex;
  justify-content: flex-end;
  align-items: center;
  width: 65%;
  @media (max-width: 400px) {
    width: 100%;
    margin-top: 8%;
  }
`
const ButtonCnt = styled.div `
  width: 5vw;
  height: 2.7vw;
  border-radius: 10px;
  background-color: ${({color}) => (color)};
  overflow: hidden;
  display: flex;
  justify-content: center;
  align-items: center;
  cursor: pointer;
  border: 1px solid #d2d2d2;
  min-width: 50px;
  min-height: 28px;
`
const Button = styled.div `
  width: 5vw;
  height: 2.7vw;
  display: flex;
  justify-content: center;
  align-items: center;
  min-width: 50px;
  min-height: 28px;
`

export const Header = (props) => {
  return(
    <Container>
      <LeftCol>
        {
          props.female
          ? (
              <ButtonCnt color={'#000'}>
                <Ripples>
                  <Button onClick={props.SetFemale}>
                    <SvgFemale width={'20px'} height={'20px'} fill={'white'} />
                  </Button>
                </Ripples>
              </ButtonCnt>
            )
          : (
              <ButtonCnt color={'#fff'}>
                <Ripples>
                  <Button onClick={props.SetFemale}>
                    <SvgFemale width={'20px'} height={'20px'} fill={'black'} />
                  </Button>
                </Ripples>
              </ButtonCnt>
            )
        }
        <EmptySpace />
        {
          props.male
          ? (
              <ButtonCnt color={'#000'}>
                <Ripples>
                  <Button onClick={props.SetMale}>
                    <SvgMale width={'20px'} height={'20px'} fill={'white'} />
                  </Button>
                </Ripples>
              </ButtonCnt>
            )
          : (
              <ButtonCnt color={'#fff'}>
                <Ripples>
                  <Button onClick={props.SetMale}>
                    <SvgMale width={'20px'} height={'20px'} fill={'black'} />
                  </Button>
                </Ripples>
              </ButtonCnt>
            )
        }

      </LeftCol>
      <RightCol>
        <SvgRefresh onClick={props.Reload} style={{ cursor: 'pointer' }} width={'20px'} height={'20px'} fill={'#000'} />
        <EmptySpace />
        <OutLineTextField
          onChangeText={(e) => props.SetEmailValue(e.target.value)}
          value={props.email}
          label={'Buscar por email...'}
        />
      </RightCol>
    </Container>
  )
}
